# ics-ans-role-conda-bot

Ansible role to install conda-bot, a GitLab bot that handles conda recipes.

## Role Variables

```yaml
conda_bot_image: registry.esss.lu.se/ics-infrastructure/conda-bot
conda_bot_tag: latest
# Number of gunicorn worker processes for handling requests.
conda_bot_workers: "{{ ansible_processor_vcpus }}"
conda_bot_log_level: info
# GitLab webhook secret
conda_bot_gl_secret: "secret"
# GitLab token to access the API
conda_bot_gl_access_token: "mytoken"
conda_bot_frontend_rule: "Host:{{ ansible_fqdn }}"
# Sentry Data Source Name (error reporting)
conda_bot_sentry_dsn: ""
# conda channel to use to build packages dependencies graph
conda_bot_conda_channel: https://artifactory.esss.lu.se/artifactory/api/conda/conda-e3
# GitLab group-id to look for conda recipes
conda_bot_recipe_group_id: "214"
# List of projects to exclude for recipe MR (comma separated strings of path_with_namespace)
conda_bot_exclude_recipe_mr_projects: ""
# GitLab project id of the repository with the global conda_build_config.yaml
conda_bot_recipe_pinning_project_id: "1399"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conda-bot
```

## License

BSD 2-clause
